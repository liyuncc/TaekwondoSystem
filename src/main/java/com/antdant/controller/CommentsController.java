package com.antdant.controller;

import com.antdant.common.GeneralResponse;
import com.antdant.entity.Comment;
import com.antdant.entity.User;
import com.antdant.service.CommentsService;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @Author Lee Yunc
 * @Date 2021/3/2 16:22
 * @Description:
 * @Version 1.0
 */
@Slf4j
@RestController
@RequestMapping("/comments")
public class CommentsController extends BaseController {

    @Resource
    private CommentsService commentsService;

    /**
     * 返回评论分页数据
     *
     * @param current
     * @param size
     * @param comment
     * @return
     */
    @RequestMapping(value = "/getComments", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public GeneralResponse getComments(Integer current, Integer size, @RequestBody Comment comment) {
        return success(commentsService.getAllComments(current, size, comment));
    }

    /**
     * 根据评论id删除评论
     *
     * @param commentId
     * @return
     */
    @RequestMapping(value = "/deleteComment")
    public GeneralResponse deleteComment(Integer commentId) {
        return success(commentsService.deleteCommentById(commentId));
    }

    /**
     * 点赞+1
     *
     * @param commentId
     * @return
     */
    @RequestMapping(value = "/addSupport")
    private GeneralResponse addSupport(Integer commentId) {
        return success(commentsService.addSupport(commentId));
    }

    /**
     * 新增一条评论
     *
     * @param comment
     * @param request
     * @return
     */
    @RequestMapping(value = "/addNewComment", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public GeneralResponse addNewComment(@RequestBody Comment comment, HttpServletRequest request) {
        User user = getLoginUser(request);
        return success(commentsService.addNewComment(comment, user));
    }

}
