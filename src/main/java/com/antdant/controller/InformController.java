package com.antdant.controller;

import com.antdant.common.GeneralResponse;
import com.antdant.entity.Apply;
import com.antdant.entity.Inform;
import com.antdant.entity.User;
import com.antdant.service.InformService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @Author liyunc
 * @Date 2021/6/5 13:56
 * @Description:
 * @Version 1.0
 */
@Slf4j
@RestController
@RequestMapping("/inform")
public class InformController extends BaseController {

    @Resource
    private InformService informService;

    /**
     * 添加新公告
     *
     * @param inform
     * @return
     */
    @RequestMapping("/addNewInform")
    public GeneralResponse addNewInform(@RequestBody Inform inform) {
        return success(informService.addNewInform(inform));
    }

    @RequestMapping("/getInformInfo")
    public GeneralResponse getInformInfo() {
        return success(informService.getInformInfo());
    }

    @RequestMapping("/createApply")
    public GeneralResponse createApply(@RequestBody Apply apply, HttpServletRequest request) {
        User user = getLoginUser(request);//获取当前申请用户
        apply.setApplyFromId(user.getId());
        apply.setApplyUserName(user.getUsername());
        return success(informService.createApply(apply));
    }
}
