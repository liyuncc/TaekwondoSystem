package com.antdant.controller;

import com.antdant.common.GeneralResponse;
import com.antdant.entity.User;
import com.antdant.exception.GeneralException;
import com.antdant.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @Author Lee Yunc
 * @Date 2021/2/25 16:50
 * @Description:
 * @Version 1.0
 */
@Slf4j
@RestController
@RequestMapping("/user")
public class UserController extends BaseController {

    @Resource
    private UserService userService;

    /**
     * 用户注册
     *
     * @param user
     * @return
     */
    @RequestMapping(value = "/register", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public GeneralResponse register(@RequestBody User user) {
        try {
            return success(userService.register(user));
        } catch (GeneralException e) {
            log.error("register failed,Error:", e);
            return failed(e.getMessage());
        } catch (Exception e) {
            log.error("register failed,Error:", e);
            return failed("注册失败，请稍后重试");
        }
    }

    /**
     * 删除用户
     *
     * @param userId
     * @return
     */
    @RequestMapping(value = "/delete")
    public GeneralResponse delete(@RequestParam Integer userId) {
        return success(userService.delete(userId));
    }

    /**
     * 修改用户信息
     *
     * @param user
     * @return
     */
    @RequestMapping(value = "/update")
    public GeneralResponse update(@RequestBody User user) {
        return success(userService.modifyUser(user));
    }

    /**
     * 根据id查找用户
     *
     * @param userId
     * @return
     */
    @RequestMapping(value = "/getUserById")
    public GeneralResponse getUserById(@RequestParam Integer userId) {
        return success(userService.getUserById(userId));
    }

    /**
     * 按条件 分页 返回学生数据
     *
     * @param current
     * @param size
     * @return
     */
    @RequestMapping(value = "/getAllUser")
    public GeneralResponse getAllUser(Integer current, Integer size, @RequestBody User user) {
        return success(userService.getAllUser(current, size, user));
    }

    /**
     * 重置密码
     *
     * @param loginId
     * @param password
     * @return
     */
    @RequestMapping(value = "/updatePassword")
    public GeneralResponse updatePassword(@RequestParam String loginId, @RequestParam String password) {
        return success(userService.updatePassword(loginId, password));
    }

    /**
     * 用户授权
     *
     * @param userId
     * @return
     */
    @RequestMapping(value = "/beManager")
    public GeneralResponse beManager(@RequestParam Integer userId) {
        return success(userService.toBeManager(userId));
    }
}
