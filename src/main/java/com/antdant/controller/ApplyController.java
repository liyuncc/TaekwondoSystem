package com.antdant.controller;

import com.antdant.common.GeneralResponse;
import com.antdant.entity.Apply;
import com.antdant.entity.User;
import com.antdant.service.ApplyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @Author Lee Yunc
 * @Date 2021/3/8 11:32
 * @Description:
 * @Version 1.0
 */
@Slf4j
@RestController
@RequestMapping("/apply")
public class ApplyController extends BaseController {

    @Resource
    private ApplyService applyService;

    /**
     * 提交申请
     *
     * @param apply
     * @param request
     * @return
     */
    @RequestMapping(value = "/createApply", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public GeneralResponse addApply(@RequestBody Apply apply, HttpServletRequest request) {
        User user = getLoginUser(request);//获取当前申请用户
        apply.setApplyFromId(user.getId());
        apply.setApplyUserName(user.getUsername());
        return success(applyService.addApply(apply));
    }

    /**
     * 管理员获取所在课程收到的  申请信息
     * 普通用户通过自己的id获取自己的申请信息
     *
     * @param current
     * @param size
     * @param request
     * @return
     */
    @RequestMapping(value = "/getApply", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public GeneralResponse getApply(@RequestParam Integer current, @RequestParam Integer size, HttpServletRequest request) {
        User user = getLoginUser(request);
        log.info("getApply,当前用户为：{}",user.toString());
        if (user.getIsManager() > 0) {
            return success(applyService.getAllApply(current, size, user.getId()));
        } else {
            return success(applyService.getApplyById(current, size, user.getId()));
        }
    }

    /**
     * 通过申请
     *
     * @param apply
     * @return
     */
    @RequestMapping(value = "/passApply", method = RequestMethod.POST)
    public GeneralResponse passApply(@RequestBody Apply apply) {
        return success(applyService.passApply(apply));
    }

    /**
     * 拒绝申请
     *
     * @param apply
     * @return
     */
    @RequestMapping(value = "/refuseApply", method = RequestMethod.POST)
    public GeneralResponse refuseApply(@RequestBody Apply apply) {
        return success(applyService.refuseApply(apply));
    }

    /**
     * 删除申请
     *
     * @param apply
     * @return
     */
    @RequestMapping(value = "/deleteApply", method = RequestMethod.POST)
    public GeneralResponse deleteApply(@RequestBody Apply apply) {
        return success(applyService.deleteApply(apply));
    }
}
