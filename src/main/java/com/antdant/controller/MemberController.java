package com.antdant.controller;

import com.antdant.common.GeneralResponse;
import com.antdant.entity.Member;
import com.antdant.entity.User;
import com.antdant.service.MemberService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @Author liyunc
 * @Date 2021/5/26 16:47
 * @Description:
 * @Version 1.0
 */

@Slf4j
@RestController
@RequestMapping("/member")
public class MemberController extends BaseController {

    @Resource
    private MemberService memberService;

    /**
     * 新增会员
     *
     * @param member
     * @return
     */
    @RequestMapping(value = "/addNewMember", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public GeneralResponse addNewMember(@RequestBody Member member) {
        return success(memberService.addNewMember(member));
    }

    /**
     * 查询所有会员信息
     *
     * @param current
     * @param size
     * @param member
     * @return
     */
    @RequestMapping("/getAllMember")
    public GeneralResponse getAllMember(Integer current, Integer size, @RequestBody Member member) {
        return success(memberService.getAllMember(current, size, member));
    }

    /**
     * 会员续费
     * @param memberId
     * @return
     */
    @RequestMapping("/renewMember")
    public GeneralResponse renewMember(@RequestParam Integer memberId) {
        return success(memberService.renewMember(memberId));
    }
}
