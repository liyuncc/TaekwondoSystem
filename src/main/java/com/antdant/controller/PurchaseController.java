package com.antdant.controller;

import com.antdant.common.GeneralResponse;
import com.antdant.entity.Purchase;
import com.antdant.service.PurchaseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author liyunc
 * @Date 2021/5/26 16:48
 * @Description:
 * @Version 1.0
 */

@Slf4j
@RestController
@RequestMapping("/purchase")
public class PurchaseController extends BaseController {

    @Resource
    private PurchaseService purchaseService;

    /**
     * 添加教具采购信息
     *
     * @param purchase
     * @return
     */
    @RequestMapping("/addNewPurchase")
    public GeneralResponse addNewPurchase(@RequestBody Purchase purchase) {
        return success(purchaseService.addNewPurchase(purchase));
    }

    /**
     * 获取所有采购信息
     *
     * @param current
     * @param size
     * @param purchase
     * @return
     */
    @RequestMapping("/getAllPurchase")
    public GeneralResponse getAllPurchase(Integer current, Integer size, @RequestBody Purchase purchase) {
        return success(purchaseService.getAllPurchase(current, size, purchase));
    }

    /**
     * 删除采购记录
     *
     * @param purchaseId
     * @return
     */
    @RequestMapping("/deletePurchase")
    public GeneralResponse deletePurchase(@RequestParam Integer purchaseId) {
        return success(purchaseService.deletePurchase(purchaseId));
    }
}
