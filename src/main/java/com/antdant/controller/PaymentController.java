package com.antdant.controller;

import com.antdant.common.GeneralResponse;
import com.antdant.entity.Member;
import com.antdant.entity.Payment;
import com.antdant.entity.User;
import com.antdant.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author liyunc
 * @Date 2021/6/4 22:42
 * @Description:
 * @Version 1.0
 */
@Slf4j
@RestController
@RequestMapping("/payment")
public class PaymentController extends BaseController {

    @Resource
    private PaymentService paymentService;

    /**
     * 新增缴费会员
     *
     * @param user
     * @return
     */
    @RequestMapping(value = "/addNewUser", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public GeneralResponse addNewUser(@RequestBody User user) {
        return success(paymentService.addNewUser(user));
    }

    /**
     * 查询所有缴费记录
     *
     * @param current
     * @param size
     * @param payment
     * @return
     */
    @RequestMapping("/getAllPayment")
    public GeneralResponse getAllPayment(Integer current, Integer size, @RequestBody Payment payment) {
        return success(paymentService.getAllPayment(current, size, payment));
    }

    /**
     * 新增缴费记录
     * @param payment
     * @return
     */
    @RequestMapping("/addNewPayment")
    public GeneralResponse addNewPayment(@RequestBody Payment payment) {
        return success(paymentService.addNewPayment(payment));
    }
}
