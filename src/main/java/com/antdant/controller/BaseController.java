package com.antdant.controller;

import com.antdant.annotation.RequireLogin;
import com.antdant.common.GeneralCode;
import com.antdant.common.GeneralResponse;
import com.antdant.entity.User;
import com.antdant.service.LoginService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @Author Lee Yunc
 * @Date 2021/2/23 14:35
 * @Description:
 * @Version 1.0
 */
public class BaseController {

    @Resource
    private LoginService loginService;

    /**
     * 获取登录用户信息
     *
     * @param request
     * @return User
     */
    protected User getLoginUser(HttpServletRequest request) {
        return loginService.getLoginUser(loginService.getUserTokenHeader(request));
    }

    /**
     * 成功返回无数据
     *
     * @return
     */
    protected GeneralResponse success() {
        return GeneralResponse.success();
    }

    /**
     * 成功返回数据
     *
     * @param data 待返回数据
     * @return
     */
    protected GeneralResponse success(Object data) {
        return GeneralResponse.success(data);
    }

    /**
     * 成功返回信息
     *
     * @param message 待返回信息
     * @return
     */
    protected GeneralResponse success(String message) {
        return GeneralResponse.success(message);
    }

    /**
     * 成功返回消息和数据
     *
     * @param message 待返回消息
     * @param data    待返回数据
     * @return
     */
    protected GeneralResponse success(String message, Object data) {
        return GeneralResponse.success(message, data);
    }

    /**
     * 失败返回
     *
     * @return
     */
    protected GeneralResponse failed() {
        return GeneralResponse.failed();
    }

    /**
     * 失败返回数据
     *
     * @param data 待返回数据
     * @return
     */
    protected GeneralResponse failed(Object data) {
        return GeneralResponse.failed(data);
    }

    /**
     * 失败返回消息
     *
     * @param message 待返回信息
     * @return
     */
    protected GeneralResponse failed(String message) {
        return GeneralResponse.failed(message);
    }

    /**
     * 返回失败消息和数据
     *
     * @param message 待返回消息
     * @param data    待返回数据
     * @return
     */
    protected GeneralResponse failed(String message, Object data) {
        return GeneralResponse.failed(message, data);
    }

    /**
     * 失败返回状态码和信息
     *
     * @param code    待返回状态码
     * @param message 待返回信息
     * @return
     */
    protected GeneralResponse failed(GeneralCode code, String message) {
        return GeneralResponse.failed(code, message);
    }

    protected GeneralResponse failed(int code, String message) {
        return GeneralResponse.failed(code, message);
    }

}
