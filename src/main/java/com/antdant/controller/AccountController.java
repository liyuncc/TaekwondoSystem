package com.antdant.controller;

import com.antdant.common.GeneralResponse;
import com.antdant.entity.Account;
import com.antdant.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author liyunc
 * @Date 2021/5/26 16:47
 * @Description:
 * @Version 1.0
 */

@Slf4j
@RestController
@RequestMapping("/account")
public class AccountController extends BaseController {

    @Resource
    private AccountService accountService;

    @RequestMapping("/getAllAccount")
    public GeneralResponse getAllAccount(Integer current, Integer size, @RequestBody Account account) {
        return success(accountService.getAllAccount(current,size,account));
    }
}
