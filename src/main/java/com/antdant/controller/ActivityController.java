package com.antdant.controller;

import com.antdant.common.GeneralResponse;
import com.antdant.entity.Activity;
import com.antdant.service.ActivityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @Author Lee Yunc
 * @Date 2021/3/8 10:18
 * @Description: 活动管理
 * @Version 1.0
 */
@Slf4j
@RestController
@RequestMapping("/activity")
public class ActivityController extends BaseController {

    @Resource
    private ActivityService activityService;

    /**
     * 获取活动列表 可选条件：课程id
     *
     * @param current
     * @param size
     * @return
     */
    @RequestMapping(value = "/getAllActivity", method = RequestMethod.GET)
    public GeneralResponse getAllActivity(@RequestParam Integer current, @RequestParam Integer size, @RequestParam Integer courseId) {
        return success(activityService.getAllActivity(current, size, courseId));
    }

    /**
     * 根据id获取活动
     *
     * @param activityId
     * @return
     */
    @RequestMapping(value = "/getActivityById", method = RequestMethod.GET)
    public GeneralResponse getActivityById(@RequestParam Integer activityId) {
        return success(activityService.getActivityById(activityId));
    }

    /**
     * 管理页根据查询条件返回活动
     *
     * @param current
     * @param size
     * @param activity
     * @return
     */
    @RequestMapping(value = "/getManageActivity", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public GeneralResponse getManageActivity(Integer current, Integer size, @RequestBody Activity activity) {
        return success(activityService.getManageActivity(current, size, activity));
    }

    /**
     * 根据id删除活动
     *
     * @param activityId
     * @return
     */
    @RequestMapping("/deleteActivity")
    public GeneralResponse deleteActivity(@RequestParam Integer activityId) {
        return success(activityService.deleteActivity(activityId));
    }

    /**
     * 新增一条活动记录
     *
     * @param activity
     * @return
     */
    @RequestMapping(value = "/addNewAc", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public GeneralResponse addNewActivity(@RequestBody Activity activity) {
        log.info("newAC：" + activity.toString());
        return success(activityService.addNewActivity(activity));
    }

}
