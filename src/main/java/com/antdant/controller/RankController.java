package com.antdant.controller;

import com.antdant.common.GeneralResponse;
import com.antdant.entity.Rank;
import com.antdant.entity.User;
import com.antdant.service.RankService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @Author Lee Yunc
 * @Date 2021/3/9 9:28
 * @Description: 跆拳道等级(rank grade)
 * @Version 1.0
 */
@Slf4j
@RestController
@RequestMapping("/rank")
public class RankController extends BaseController {

    @Resource
    private RankService rankService;

    /**
     * 用户加载自己段位信息
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/getUserRank", method = RequestMethod.GET)
    public GeneralResponse getUserRank(HttpServletRequest request) {
        User user = getLoginUser(request);
        return success(rankService.getRankByRankCode(user.getRankCode()));
    }

    /**
     * 根据相应的rankCode查询段位信息
     *
     * @param rankCode
     * @return
     */
    @RequestMapping(value = "/getRankByCode", method = RequestMethod.GET)
    public GeneralResponse getRankByCode(@RequestParam Integer rankCode) {
        return success(rankService.getRankByRankCode(rankCode));
    }

    /**
     * 分页查询所有段位信息
     *
     * @param current
     * @param size
     * @return
     */
    @RequestMapping(value = "/getAllRanks", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public GeneralResponse getAllRanks(@RequestParam Integer current, @RequestParam Integer size, @RequestBody Rank rank) {
        return success(rankService.getRanks(current, size, rank));
    }
}
