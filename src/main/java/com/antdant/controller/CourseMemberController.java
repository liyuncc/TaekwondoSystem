package com.antdant.controller;

import com.antdant.common.GeneralResponse;
import com.antdant.entity.CourseMember;
import com.antdant.entity.User;
import com.antdant.service.CourseMemberService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @Author Lee Yunc
 * @Date 2021/3/2 10:00
 * @Description: 课程成员管理
 * @Version 1.0
 */
@Slf4j
@RestController
@RequestMapping("/courseMember")
public class CourseMemberController extends BaseController {

    @Resource
    private CourseMemberService courseMemberService;

    /**
     * 判断某个用户是否在这个课程里
     *
     * @param courseMember
     * @param request
     * @return
     */
    @RequestMapping(value = "/isInCourse", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public GeneralResponse isInThisCourse(@RequestBody CourseMember courseMember, HttpServletRequest request) {
        User user = getLoginUser(request);//获取当前用户
        return success(courseMemberService.isInThisCourse(user.getId(), courseMember.getCourseId()));
    }

    /**
     * 管理页获取关联表数据
     *
     * @param current
     * @param size
     * @param courseMember
     * @return
     */
    @RequestMapping(value = "/getManageCM")
    public GeneralResponse getManageCourseMember(Integer current, Integer size, @RequestBody CourseMember courseMember) {
        return success(courseMemberService.getManageCourseMember(current, size, courseMember));
    }

    /**
     * 删除关联
     *
     * @param userId    用户id
     * @param courseId  课程id
     * @param isManager
     * @return
     */
    @RequestMapping(value = "/deleteRelate")
    public GeneralResponse deleteRelate(@RequestParam Integer userId, @RequestParam Integer courseId, @RequestParam Integer isManager) {
        return success(courseMemberService.deleteRelate(userId, courseId, isManager));
    }


}
