package com.antdant.controller;

import com.antdant.common.GeneralResponse;
import com.antdant.entity.Certificate;
import com.antdant.service.CertificateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @Author Lee Yunc
 * @Date 2021/3/17 16:58
 * @Description:
 * @Version 1.0
 */
@Slf4j
@RestController
@RequestMapping("/certificate")
public class CertificateController extends BaseController {

    @Resource
    private CertificateService certificateService;

    /**
     * 用户查询自己证书情况
     *
     * @param certificate
     * @return
     */
    @RequestMapping(value = "/getCertificate", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public GeneralResponse getCertificate(@RequestBody Certificate certificate) {
        return success(certificateService.getCertificate(certificate));
    }

    /**
     * 分页查询所有证书信息
     *
     * @param current
     * @param size
     * @param certificate
     * @return
     */
    @RequestMapping(value = "/getAllCertificates", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public GeneralResponse getAllCertificates(@RequestParam Integer current, @RequestParam Integer size, @RequestBody Certificate certificate) {
        return success(certificateService.getAllCertificates(current, size, certificate));
    }

    /**
     * 根据id删除证书
     *
     * @param certificateId
     * @return
     */
    @RequestMapping(value = "/deleteCertificate", method = RequestMethod.GET)
    public GeneralResponse deleteCertificate(@RequestParam Integer certificateId) {
        return success(certificateService.deleteCertificate(certificateId));
    }

    /**
     * 添加新证书
     *
     * @param certificate
     * @return
     */
    @RequestMapping(value = "/addNewCertificate", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public GeneralResponse addNewCertificate(@RequestBody Certificate certificate) {
        return success(certificateService.addNewCertificate(certificate));
    }
}
