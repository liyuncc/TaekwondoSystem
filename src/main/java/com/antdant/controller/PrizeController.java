package com.antdant.controller;

import com.antdant.common.GeneralResponse;
import com.antdant.entity.Prize;
import com.antdant.service.PrizeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @Author Lee Yunc
 * @Date 2021/3/16 15:31
 * @Description:
 * @Version 1.0
 */
@Slf4j
@RestController
@RequestMapping("/prize")
public class PrizeController extends BaseController {

    @Resource
    private PrizeService prizeService;

    /**
     * 奖品分页返回
     *
     * @param current
     * @param size
     * @param prizeName
     * @return
     */
    @RequestMapping(value = "/getAllPrizes", method = RequestMethod.GET)
    public GeneralResponse getAllPrizes(@RequestParam Integer current, @RequestParam Integer size,
                                        @RequestParam(value = "prizeName", required = false, defaultValue = "") String prizeName) {
        log.info("prizeName" + prizeName);
        return success(prizeService.getAllPrizes(current, size, prizeName));
    }

    /**
     * 增加新奖品
     *
     * @param prize
     * @return
     */
    @RequestMapping(value = "/addNewPrize", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public GeneralResponse addNewPrize(@RequestBody Prize prize) {
        return success(prizeService.addNewPrize(prize));
    }

    /**
     * 根据id删除奖品
     * @param prizeId
     * @return
     */
    @RequestMapping(value = "/deletePrize", method = RequestMethod.GET)
    public GeneralResponse deletePrize(@RequestParam Integer prizeId) {
        return success((prizeService.deletePrize(prizeId)));
    }


}
