package com.antdant.controller;

import com.antdant.common.GeneralResponse;
import com.antdant.entity.CreateCourse;
import com.antdant.entity.User;
import com.antdant.service.CreateCourseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @Author Lee Yunc
 * @Date 2021/3/2 15:41
 * @Description: 创建课程
 * @Version 1.0
 */
@Slf4j
@RestController
@RequestMapping("/createCourse")
public class CreateCourseController extends BaseController {

    @Resource
    private CreateCourseService createCourseService;

    /**
     * 创建一个新课程
     *
     * @param createCourse
     * @param request
     * @return
     */
    @RequestMapping(value = "/createCourseApply", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public GeneralResponse createNewCourseApply(@RequestBody CreateCourse createCourse, HttpServletRequest request) {
        User user = getLoginUser(request);
        createCourse.setApplyFromId(user.getId());//获取当前用户id
        return success(createCourseService.createNewCourseApply(createCourse));
    }

    /**
     * 获取所有的创建请求
     *
     * @param current
     * @param size
     * @param createCourse
     * @return
     */
    @RequestMapping(value = "/getAllCreateApply", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public GeneralResponse getAllCreateApply(Integer current, Integer size, @RequestBody CreateCourse createCourse) {
        return success(createCourseService.getAllCreateApply(current, size, createCourse));
    }

    /**
     * 通过创建课程的申请
     *
     * @param applyId
     * @return
     */
    @RequestMapping("/passCreateCourseApply")
    public GeneralResponse passCreateCourseApply(@RequestParam Integer applyId) {
        return success(createCourseService.passCreateCourseApply(applyId));
    }

    /**
     * 拒绝创建课程的申请
     *
     * @param applyId
     * @return
     */
    @RequestMapping("/refuseCreateCourseApply")
    public GeneralResponse refuseCreateCourseApply(@RequestParam Integer applyId) {
        return success(createCourseService.refuseCreateCourseApply(applyId));
    }

    /**
     * 删除创建课程的申请
     *
     * @param applyId 申请id
     * @return
     */
    @RequestMapping("/deleteCreateCourseApply")
    public GeneralResponse deleteCreateTeamApply(@RequestParam Integer applyId) {
        return success(createCourseService.deleteCreateCourseApply(applyId));
    }
}
