package com.antdant.controller;

import com.antdant.common.GeneralResponse;
import com.antdant.entity.LuckyDraw;
import com.antdant.entity.Prize;
import com.antdant.entity.User;
import com.antdant.service.LuckyDrawService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @Author Lee Yunc
 * @Date 2021/3/15 11:20
 * @Description: 抽奖
 * @Version 1.0
 */
@Slf4j
@RestController
@RequestMapping("/draw")
public class LuckyDrawController extends BaseController {

    @Resource
    private LuckyDrawService luckyDrawService;


    /**
     * 添加抽奖记录
     *
     * @param prize
     * @param request
     * @return
     */
    @RequestMapping(value = "/addNewDrawRecord", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public GeneralResponse addNewDrawRecord(@RequestBody Prize prize, HttpServletRequest request) {
        User user = getLoginUser(request);
        return success(luckyDrawService.addNewDrawRecord(prize, user));
    }

    /**
     * 根据id删除抽奖记录
     *
     * @param drawRecordId
     * @return
     */
    @RequestMapping(value = "/deleteDrawRecord", method = RequestMethod.GET)
    public GeneralResponse deleteDrawRecord(@RequestParam Integer drawRecordId) {
        return success(luckyDrawService.deleteDrawRecord(drawRecordId));
    }

    /**
     * 根据用户id查出抽奖记录
     *
     * @param current
     * @param size
     * @return
     */
    @RequestMapping(value = "/getDrawRecords", method = RequestMethod.GET)
    public GeneralResponse getDrawRecords(@RequestParam Integer current, @RequestParam Integer size, HttpServletRequest request) {
        User user = getLoginUser(request);
        return success(luckyDrawService.getDrawRecords(current, size, user));
    }

    /**
     * 查询所有抽奖记录（管理员）
     *
     * @param current
     * @param size
     * @param luckyDraw
     * @return
     */
    @RequestMapping(value = "/getAllDrawRecords", method = RequestMethod.POST)
    public GeneralResponse getAllDrawRecords(@RequestParam Integer current, @RequestParam Integer size, @RequestBody LuckyDraw luckyDraw) {
        return success(luckyDrawService.getAllDrawRecords(current, size, luckyDraw));
    }

    /**
     * 兑换奖品
     *
     * @param drawRecordId
     * @return
     */
    @RequestMapping(value = "/updateDrawRecordStatus", method = RequestMethod.GET)
    public GeneralResponse updateDrawRecordStatus(@RequestParam Integer drawRecordId) {
        return success(luckyDrawService.updateDrawRecordStatus(drawRecordId));
    }

}
