package com.antdant.controller;

import com.antdant.common.GeneralResponse;
import com.antdant.entity.Course;
import com.antdant.entity.User;
import com.antdant.service.CourseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @Author Lee Yunc
 * @Date 2021/2/26 21:23
 * @Description: 课程管理
 * @Version 1.0
 */
@Slf4j
@RestController
@RequestMapping("/course")
public class CourseController extends BaseController {

    @Resource
    private CourseService courseService;

    /**
     * 获取课程分页列表：可选条件：课程名称
     *
     * @param pageCurrent
     * @param pageSize
     * @param courseName
     * @return
     */
    @RequestMapping(value = "/getAllCourse", method = RequestMethod.GET)
    public GeneralResponse getAllCourse(@RequestParam Integer pageCurrent, @RequestParam Integer pageSize,
                                        @RequestParam(value = "courseName", required = false, defaultValue = "") String courseName) {
        return success(courseService.getCourses(pageCurrent, pageSize, courseName));
    }

    /**
     * 管理页 按条件分页返回课程
     *
     * @param current
     * @param size
     * @param course
     * @return
     */
    @RequestMapping(value = "/getManageCourse", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public GeneralResponse getManageCourse(Integer current, Integer size, @RequestBody Course course) {
        return success(courseService.getAllCourse(current, size, course));
    }

    /**
     * 根据id查找课程
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/getCourseById", method = RequestMethod.GET)
    public GeneralResponse getCourseById(Integer id) {
        return success(courseService.getCoursesById(id));
    }

    /**
     * 根据课程类型查找课程
     *
     * @param types types = "12345",表示1类、2、3、4、5类要多条件查询
     * @return
     */
    @RequestMapping(value = "/getCoursesByType", method = RequestMethod.GET)
    public GeneralResponse getCourseByType(@RequestParam String types) {
        return success(courseService.getCoursesByType(types));
    }


    /**
     * 更新课程信息
     *
     * @param course
     * @return
     */
    @RequestMapping(value = "/updateCourseInfo")
    public GeneralResponse updateCourseInfo(@RequestBody Course course) {
        return success(courseService.updateCourseInfo(course));
    }

    /**
     * 判断当前登录用户是否是该课程的管理员
     *
     * @param courseId
     * @return
     */
    @RequestMapping(value = "/isCourseManager")
    public GeneralResponse isThisManager(@RequestParam Integer courseId, HttpServletRequest request) {
        User user = getLoginUser(request);
        return success(courseService.isThisManager(user.getId(), courseId));
    }
}
