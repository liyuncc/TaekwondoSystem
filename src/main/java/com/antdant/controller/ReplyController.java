package com.antdant.controller;

import com.antdant.common.GeneralResponse;
import com.antdant.entity.Reply;
import com.antdant.entity.User;
import com.antdant.service.ReplyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @Author Lee Yunc
 * @Date 2021/3/3 11:38
 * @Description:
 * @Version 1.0
 */
@Slf4j
@RestController
@RequestMapping("/reply")
public class ReplyController extends BaseController {

    @Resource
    private ReplyService replyService;

    /**
     * 返回回复分页数据
     *
     * @param current
     * @param size
     * @param reply
     * @return
     */
    @RequestMapping(value = "/getReply", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public GeneralResponse getReply(Integer current, Integer size, @RequestBody Reply reply) {
        return success(replyService.getAllReply(current, size, reply));
    }

    /**
     * 根据回复id删除回复
     *
     * @param replyId
     * @return
     */
    @RequestMapping(value = "/deleteReply")
    public GeneralResponse deleteReply(@RequestParam Integer replyId) {
        return success(replyService.deleteReplyById(replyId));
    }

    /**
     * 点赞+1
     *
     * @param replyId
     * @return
     */
    @RequestMapping(value = "/addSupport")
    public GeneralResponse addSupport(@RequestParam Integer replyId) {
        return success(replyService.addSupport(replyId));
    }

    /**
     * 新增一条回复
     *
     * @param reply
     * @param request
     * @return
     */
    @RequestMapping(value = "/addNewReply", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public GeneralResponse addNewReply(@RequestBody Reply reply, HttpServletRequest request) {
        User user = getLoginUser(request);
        return success(replyService.addNewReply(reply, user));
    }


}
