package com.antdant.repository;

import com.antdant.entity.LuckyDraw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @Author Lee Yunc
 * @Date 2021/3/15 17:38
 * @Description:
 * @Version 1.0
 */
@Repository
public interface LuckyDrawRepository extends JpaRepository<LuckyDraw, Integer>, JpaSpecificationExecutor<LuckyDraw> {
}
