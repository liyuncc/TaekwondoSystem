package com.antdant.repository;

import com.antdant.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @Author liyunc
 * @Date 2021/5/26 16:41
 * @Description:
 * @Version 1.0
 */
public interface MemberRepository extends JpaRepository<Member, Integer>, JpaSpecificationExecutor<Member> {
}
