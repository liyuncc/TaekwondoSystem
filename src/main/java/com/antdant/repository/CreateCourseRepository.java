package com.antdant.repository;

import com.antdant.entity.CreateCourse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @Author Lee Yunc
 * @Date 2021/3/2 15:42
 * @Description:
 * @Version 1.0
 */
@Repository
public interface CreateCourseRepository extends JpaRepository<CreateCourse, Integer>, JpaSpecificationExecutor<CreateCourse> {
}
