package com.antdant.repository;

import com.antdant.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @Author Lee Yunc
 * @Date 2021/3/2 16:25
 * @Description:
 * @Version 1.0
 */
@Repository
public interface CommentsRepository extends JpaRepository<Comment, Integer>, JpaSpecificationExecutor<Comment> {
}
