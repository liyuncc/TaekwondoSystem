package com.antdant.repository;

import com.antdant.entity.Apply;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @Author Lee Yunc
 * @Date 2021/3/8 11:36
 * @Description:
 * @Version 1.0
 */
@Repository
public interface ApplyRepository extends JpaRepository<Apply, Integer>, JpaSpecificationExecutor<Apply> {

    void deleteByApplyFromId(Integer applyFromId);
}
