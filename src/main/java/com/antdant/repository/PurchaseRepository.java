package com.antdant.repository;

import com.antdant.entity.Purchase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @Author liyunc
 * @Date 2021/5/26 16:42
 * @Description:
 * @Version 1.0
 */
public interface PurchaseRepository extends JpaRepository<Purchase, Integer>, JpaSpecificationExecutor<Purchase> {
}
