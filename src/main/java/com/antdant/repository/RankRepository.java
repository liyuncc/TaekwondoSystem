package com.antdant.repository;

import com.antdant.entity.Rank;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @Author Lee Yunc
 * @Date 2021/3/9 9:33
 * @Description:
 * @Version 1.0
 */
@Repository
public interface RankRepository extends JpaRepository<Rank, Integer>, JpaSpecificationExecutor<Rank> {

    Rank findByRankCode(Integer rankCode);

    Integer findRankCodeByGrade(String grade);
}
