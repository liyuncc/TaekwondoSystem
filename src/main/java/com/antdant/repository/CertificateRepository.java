package com.antdant.repository;

import com.antdant.entity.Certificate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @Author Lee Yunc
 * @Date 2021/3/17 17:04
 * @Description:
 * @Version 1.0
 */
@Repository
public interface CertificateRepository extends JpaRepository<Certificate, Integer>, JpaSpecificationExecutor<Certificate> {

    Certificate findByNameAndNumber(String name, String number);
}
