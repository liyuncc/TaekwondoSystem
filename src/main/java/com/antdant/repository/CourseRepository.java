package com.antdant.repository;

import com.antdant.entity.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @Author Lee Yunc
 * @Date 2021/2/26 21:24
 * @Description:
 * @Version 1.0
 */
@Repository
public interface CourseRepository extends JpaRepository<Course, Integer>, JpaSpecificationExecutor<Course> {

    void deleteCourseByCourseManagerId(Integer courseManagerId);
}
