package com.antdant.repository;

import com.antdant.entity.Reply;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @Author Lee Yunc
 * @Date 2021/3/2 17:06
 * @Description:
 * @Version 1.0
 */
@Repository
public interface ReplyRepository extends JpaRepository<Reply, Integer>, JpaSpecificationExecutor<Reply> {

    void deleteByReplyToId(Integer replyToId);
}
