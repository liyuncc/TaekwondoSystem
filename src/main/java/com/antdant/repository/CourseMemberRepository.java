package com.antdant.repository;

import com.antdant.entity.CourseMember;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @Author Lee Yunc
 * @Date 2021/3/2 10:01
 * @Description:
 * @Version 1.0
 */
@Repository
public interface CourseMemberRepository extends JpaRepository<CourseMember, Integer>, JpaSpecificationExecutor<CourseMember> {

    CourseMember findByUserIdAndCourseId(Integer userId, Integer courseId);

    CourseMember findByUserIdAndIsManager(Integer userId, Integer isManager);

    void deleteByUserIdAndCourseId(Integer userIs, Integer courseId);
}
