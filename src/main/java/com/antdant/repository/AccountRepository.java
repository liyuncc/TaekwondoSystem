package com.antdant.repository;

import com.antdant.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @Author liyunc
 * @Date 2021/5/26 16:41
 * @Description:
 * @Version 1.0
 */
public interface AccountRepository extends JpaRepository<Account, Integer>, JpaSpecificationExecutor<Account> {
}
