package com.antdant.repository;

import com.antdant.entity.Inform;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @Author liyunc
 * @Date 2021/6/5 13:12
 * @Description:
 * @Version 1.0
 */
@Repository
public interface InformRepository extends JpaRepository<Inform, Integer>, JpaSpecificationExecutor<Inform> {
}
