package com.antdant.repository;

import com.antdant.entity.Prize;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @Author Lee Yunc
 * @Date 2021/3/15 11:24
 * @Description:
 * @Version 1.0
 */
@Repository
public interface PrizeRepository extends JpaRepository<Prize, Integer>, JpaSpecificationExecutor<Prize> {
}
