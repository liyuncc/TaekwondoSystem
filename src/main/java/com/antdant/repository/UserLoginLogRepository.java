package com.antdant.repository;

import com.antdant.entity.UserLoginLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Author Lee Yunc
 * @Date 2021/2/24 10:00
 * @Description:
 * @Version 1.0
 */
@Repository
public interface UserLoginLogRepository extends JpaRepository<UserLoginLog, Integer> {

    @Transactional
    boolean deleteByUserLoginId(String userLoginId);
}
