package com.antdant.repository;

import com.antdant.entity.Payment;
import com.antdant.entity.Prize;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @Author liyunc
 * @Date 2021/6/4 23:08
 * @Description:
 * @Version 1.0
 */
@Repository
public interface PaymentRepository extends JpaRepository<Payment, Integer>, JpaSpecificationExecutor<Payment> {
}
