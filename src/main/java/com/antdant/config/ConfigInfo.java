package com.antdant.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @Author Lee Yunc
 * @Date 2021/2/24 16:08
 * @Description: 配置信息
 * @Version 1.0
 */
@Configuration
@Data
public class ConfigInfo {

    @Value("${spring.mail.username}")
    private String emailFromUser;

    @Value("${path.filePath}")
    private String path;
}
