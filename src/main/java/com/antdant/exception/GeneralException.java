package com.antdant.exception;

import com.antdant.common.GeneralCode;
import org.apache.logging.log4j.Level;

/**
 * @Author Lee Yunc
 * @Date 2021/2/23 15:02
 * @Description:
 * @Version 1.0
 */
public class GeneralException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    protected Object data = "";

    protected int code = GeneralCode.FAILED.getCode();

    protected Level logLevel = Level.ERROR;

    public Object getData() {
        return data;
    }

    public int getCode() {
        return code;
    }

    public Level getLogLevel() {
        return logLevel;
    }

    public GeneralException(String message) {
        super(message);
    }

    public GeneralException(String message, Object data) {
        super(message);
        this.data = data;
    }

    public GeneralException(int code, String message, Object data) {
        super(message);
        this.code = code;
        this.data = data;

    }

}
