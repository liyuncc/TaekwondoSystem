package com.antdant.exception;

import com.antdant.common.GeneralCode;

/**
 * @Author Lee Yunc
 * @Date 2021/2/23 15:02
 * @Description:
 * @Version 1.0
 */
public class NeedLoginException extends GeneralException {

    public NeedLoginException(String message) {
        super(message);
        this.code = GeneralCode.NEED_LOGIN.getCode();
    }
}
