package com.antdant.service;

import com.antdant.entity.Course;
import com.antdant.entity.CourseMember;
import com.antdant.entity.CreateCourse;
import com.antdant.entity.User;
import com.antdant.repository.CreateCourseRepository;
import com.antdant.utils.DataUtils;
import com.antdant.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Author Lee Yunc
 * @Date 2021/3/2 15:42
 * @Description:
 * @Version 1.0
 */
@Slf4j
@Service
public class CreateCourseService {

    @Resource
    private CreateCourseRepository createCourseRepository;

    @Resource
    private UserService userService;

    @Resource
    private CommentsService commentsService;

    @Resource
    private ReplyService replyService;

    @Resource
    private CourseService courseService;

    @Resource
    private CourseMemberService courseMemberService;

    /**
     * 新建课程初始化
     *
     * @return
     */
    private CreateCourse createNewCourse() {
        CreateCourse createCourse = new CreateCourse();
        createCourse.setCreateTime(DateUtils.getNowTime());
        createCourse.setIsPass(0);
        return createCourse;
    }

    /**
     * 创建课程
     *
     * @param createCourse
     * @return
     */
    public CreateCourse createNewCourseApply(CreateCourse createCourse) {
        CreateCourse newCourse = createNewCourse();
        newCourse.setApplyFromId(createCourse.getApplyFromId());
        newCourse.setApplyToName(createCourse.getApplyToName());
        newCourse.setContact(createCourse.getContact());
        newCourse.setCourseType(createCourse.getCourseType());
        newCourse.setReason(createCourse.getReason());
        return createCourseRepository.saveAndFlush(newCourse);
    }

    /**
     * 返回所有创建请求
     *
     * @param current
     * @param size
     * @param createCourse
     * @return
     */
    public Page<CreateCourse> getAllCreateApply(Integer current, Integer size, CreateCourse createCourse) {
        Pageable pageable = PageRequest.of(current - 1, size);
        return createCourseRepository.findAll(new Specification<CreateCourse>() {
            @Override
            public Predicate toPredicate(Root<CreateCourse> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> list = new ArrayList<>();
                if (DataUtils.isNotEmptyStr(createCourse.getApplyToName())) {
                    list.add(criteriaBuilder.like(root.get("applyToName"), createCourse.getApplyToName()));
                }
                if (createCourse.getCourseType() != 0) {
                    list.add(criteriaBuilder.equal(root.get("courseType"), createCourse.getCourseType()));
                }
                if (createCourse.getIsPass() != -1) {
                    list.add(criteriaBuilder.equal(root.get("isPass"), createCourse.getIsPass()));
                }
                return criteriaBuilder.and(list.toArray(new Predicate[list.size()]));
            }
        }, pageable);
    }

    /**
     * 通过申请
     *
     * @param applyId
     * @return
     */
    public CreateCourse passCreateCourseApply(Integer applyId) {
        CreateCourse createCourse = createCourseRepository.findById(applyId).orElse(null);
        assert createCourse != null;
        User user = userService.toBeManager(createCourse.getApplyFromId());
        commentsService.toBeManager(createCourse.getApplyFromId());
        replyService.toBeManager(createCourse.getApplyFromId());
        Course newCourse = courseService.createNewCourse(createCourse);
        CourseMember courseMember = new CourseMember();
        courseMember.setUserId(user.getId());
        courseMember.setUserName(user.getUsername());
        courseMember.setIsManager(1);
        courseMember.setCourseId(newCourse.getId());
        courseMember.setCourseName(newCourse.getCourseName());
        courseMember.setCreateTime(DateUtils.getNowTime());
        courseMemberService.addCourseMember(courseMember);
        createCourse.setIsPass(2);//通过申请
        return createCourseRepository.saveAndFlush(createCourse);//更新状态
    }

    /**
     * 拒绝申请
     *
     * @param applyId
     * @return
     */
    public CreateCourse refuseCreateCourseApply(Integer applyId) {
        CreateCourse createCourse = createCourseRepository.findById(applyId).orElse(null);
        assert createCourse != null;
        createCourse.setIsPass(1);//拒绝申请
        return createCourseRepository.saveAndFlush(createCourse);
    }

    /**
     * 删除申请
     *
     * @param applyId
     * @return
     */
    public boolean deleteCreateCourseApply(Integer applyId) {
        try {
            createCourseRepository.deleteById(applyId);
            return true;
        } catch (RuntimeException e) {
            return false;
        }
    }
}
