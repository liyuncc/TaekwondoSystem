package com.antdant.service;

import com.antdant.entity.Account;
import com.antdant.entity.Payment;
import com.antdant.entity.User;
import com.antdant.repository.AccountRepository;
import com.antdant.repository.PaymentRepository;
import com.antdant.utils.DataUtils;
import com.antdant.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author liyunc
 * @Date 2021/6/4 22:44
 * @Description:
 * @Version 1.0
 */
@Slf4j
@Service
public class PaymentService {

    @Resource
    private UserService userService;

    @Resource
    private PaymentRepository paymentRepository;

    @Resource
    private AccountRepository accountRepository;

    /**
     * 新增缴费学员
     *
     * @param user
     * @return
     */
    public Payment addNewUser(User user) {
        Payment payment = createNewPayment();
        payment.setLoginId(user.getLoginId());
        payment.setUserName(user.getUsername());
        payment.setPhone(userService.getUserByLoginId(user.getLoginId()).getPhone());
        return paymentRepository.saveAndFlush(payment);
    }

    //缴费学员初始化
    public Payment createNewPayment() {
        Payment payment = new Payment();
        return payment;
    }

    /**
     * 查询所有缴费记录
     *
     * @param current
     * @param size
     * @param payment
     * @return
     */
    public Page<Payment> getAllPayment(Integer current, Integer size, Payment payment) {
        Pageable pageable = PageRequest.of(current - 1, size);
        return paymentRepository.findAll(new Specification<Payment>() {
            @Override
            public Predicate toPredicate(Root<Payment> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> list = new ArrayList<>();
                if (DataUtils.isNotEmptyStr(payment.getLoginId())) {
                    list.add(criteriaBuilder.like(root.get("loginId"), payment.getLoginId()));
                }
                if (DataUtils.isNotEmptyStr(payment.getUserName())) {
                    list.add(criteriaBuilder.like(root.get("userName"), payment.getUserName()));
                }
                return criteriaBuilder.and(list.toArray(new Predicate[list.size()]));
            }
        }, pageable);
    }

    /**
     * 添加缴费记录
     *
     * @param payment
     * @return
     */
    public Payment addNewPayment(Payment payment) {
        payment.setLastTime(DateUtils.getNowTime());
        //TODO 将缴费记录写入财务账单
        Account account = new Account();
        account.setLoginId(payment.getLoginId());
        account.setPhone(userService.getUserByLoginId(payment.getLoginId()).getPhone());
        account.setType(0);//0 课程缴费
        account.setUserName(payment.getUserName());
        account.setCreateTime(DateUtils.getNowTime());
        int money = 0;
        switch (payment.getLastType()) {
            case 0:
                money = 100;
                break;
            case 1:
                money = 200;
                break;
            case 2:
                money = 300;
                break;
            case 3:
                money = 400;
                break;
        }
        account.setMoney(money);
        accountRepository.saveAndFlush(account);


        return paymentRepository.saveAndFlush(payment);
    }
}
