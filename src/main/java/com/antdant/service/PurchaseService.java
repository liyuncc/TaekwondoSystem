package com.antdant.service;

import com.antdant.entity.Account;
import com.antdant.entity.Purchase;
import com.antdant.entity.User;
import com.antdant.repository.AccountRepository;
import com.antdant.repository.PurchaseRepository;
import com.antdant.utils.DataUtils;
import com.antdant.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author liyunc
 * @Date 2021/6/4 16:29
 * @Description:
 * @Version 1.0
 */
@Slf4j
@Service
public class PurchaseService {

    @Resource
    private UserService userService;

    @Resource
    private PurchaseRepository purchaseRepository;

    @Resource
    private AccountRepository accountRepository;

    /**
     * 新增缴费记录
     *
     * @param purchase
     * @return
     */
    public Purchase addNewPurchase(Purchase purchase) {
        Purchase newPurchase = createNewPurchase();
        newPurchase.setLoginId(purchase.getLoginId());
        User user = userService.getUserByLoginId(purchase.getLoginId());
        newPurchase.setUserName(user.getUsername());
        newPurchase.setPhone(user.getPhone());
        newPurchase.setMoney(purchase.getMoney());
        newPurchase.setGoodsName(purchase.getGoodsName());
        newPurchase.setCreateTime(purchase.getCreateTime());

        Account account = new Account();
        account.setLoginId(purchase.getLoginId());
        account.setPhone(userService.getUserByLoginId(purchase.getLoginId()).getPhone());
        account.setType(2);//2 教具采购
        account.setUserName(purchase.getUserName());
        account.setCreateTime(DateUtils.getNowTime());
        account.setMoney(purchase.getMoney());
        accountRepository.saveAndFlush(account);

        return purchaseRepository.saveAndFlush(newPurchase);
    }

    //初始化
    private Purchase createNewPurchase() {
        Purchase purchase = new Purchase();
        return purchase;
    }

    /**
     * 查询所有采购记录
     *
     * @param current
     * @param size
     * @param purchase
     * @return
     */
    public Page<Purchase> getAllPurchase(Integer current, Integer size, Purchase purchase) {
        Sort sort = Sort.by(Sort.Direction.DESC, "createTime");
        Pageable pageable = PageRequest.of(current - 1, size, sort);
        return purchaseRepository.findAll(new Specification<Purchase>() {
            @Override
            public Predicate toPredicate(Root<Purchase> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> list = new ArrayList<>();
                if (DataUtils.isNotEmptyStr(purchase.getLoginId())) {
                    list.add(criteriaBuilder.like(root.get("loginId"), purchase.getLoginId()));
                }
                if (DataUtils.isNotEmptyStr(purchase.getUserName())) {
                    list.add(criteriaBuilder.like(root.get("userName"), purchase.getUserName()));
                }
                if (DataUtils.isNotEmptyStr(purchase.getGoodsName())) {
                    list.add(criteriaBuilder.like(root.get("goodsName"), purchase.getGoodsName()));
                }
                return criteriaBuilder.and(list.toArray(new Predicate[list.size()]));
            }
        }, pageable);
    }

    /**
     * 删除采购记录
     *
     * @param purchaseId
     * @return
     */
    public Boolean deletePurchase(Integer purchaseId) {
        try {
            purchaseRepository.deleteById(purchaseId);
            return true;
        } catch (RuntimeException e) {
            log.error("purchase delete failed,Error:", e);
            return false;
        }
    }
}
