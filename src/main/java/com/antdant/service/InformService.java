package com.antdant.service;

import com.antdant.entity.Apply;
import com.antdant.entity.Inform;
import com.antdant.repository.ApplyRepository;
import com.antdant.repository.InformRepository;
import com.antdant.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @Author liyunc
 * @Date 2021/6/5 13:12
 * @Description:
 * @Version 1.0
 */
@Slf4j
@Service
public class InformService {

    @Resource
    private InformRepository informRepository;

    @Resource
    private ApplyRepository applyRepository;

    /**
     * 添加新公告
     *
     * @param inform
     * @return
     */
    public Inform addNewInform(Inform inform) {
        inform.setCreateTime(DateUtils.getNowTime());
        return informRepository.saveAndFlush(inform);
    }

    /**
     * 根据时间获取最新公告信息
     *
     * @return
     */
    public Inform getInformInfo() {
        Sort sort = Sort.by(Sort.Direction.DESC, "createTime");//根据点赞降序排列
        return informRepository.findAll(sort).get(0);
    }

    /**
     * 创建考级申请
     *
     * @param apply
     * @return
     */
    public Apply createApply(Apply apply) {
        Apply newApply = new Apply();
        newApply.setApplyTime(DateUtils.getNowTime());
        newApply.setIsPass(0);
        newApply.setApplyUserName(apply.getApplyUserName());
        newApply.setApplyFromId(apply.getApplyFromId());
        //TODO 考级申请审批设置有bug
        newApply.setApplyToId(1);//超级管理员审批
        newApply.setContactWay(apply.getContactWay());
        newApply.setReason(apply.getReason());
        return applyRepository.saveAndFlush(newApply);
    }
}
