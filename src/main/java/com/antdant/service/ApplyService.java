package com.antdant.service;

import com.antdant.entity.Apply;
import com.antdant.entity.CourseMember;
import com.antdant.repository.ApplyRepository;
import com.antdant.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * @Author Lee Yunc
 * @Date 2021/3/8 11:35
 * @Description:
 * @Version 1.0
 */
@Slf4j
@Service
public class ApplyService {

    @Resource
    private ApplyRepository applyRepository;

    @Resource
    private CourseMemberService courseMemberService;

    @Resource
    private CourseService courseService;

    /**
     * 新建申请
     *
     * @param apply
     * @return
     */
    public Apply addApply(Apply apply) {
        Apply newApply = createNewApply();
        newApply.setApplyUserName(apply.getApplyUserName());
        newApply.setApplyFromId(apply.getApplyFromId());
        newApply.setApplyToId(apply.getApplyToId());
        newApply.setContactWay(apply.getContactWay());
        newApply.setReason(apply.getReason());
        return applyRepository.saveAndFlush(newApply);
    }

    /**
     * 新建申请初始化
     *
     * @return
     */
    private Apply createNewApply() {
        Apply apply = new Apply();
        apply.setApplyTime(DateUtils.getNowTime());
        apply.setIsPass(0);
        return apply;
    }

    /**
     * 管理员获取想要加入课程的信息
     *
     * @param current
     * @param size
     * @param userId
     * @return
     */
    public Page<Apply> getAllApply(Integer current, Integer size, Integer userId) {
        Integer courseId = courseMemberService.getCourseIdByUserId(userId);
        Sort sort = Sort.by(Sort.Direction.DESC, "applyTime");
        Pageable pageable = PageRequest.of(current - 1, size, sort);
        return applyRepository.findAll(new Specification<Apply>() {
            @Override
            public Predicate toPredicate(Root<Apply> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Predicate temp = criteriaBuilder.equal(root.get("applyToId"), courseId);
                return temp;
            }
        }, pageable);
    }

    /**
     * 普通用户查看自己的申请消息
     *
     * @param current
     * @param size
     * @param userId
     * @return
     */
    public Page<Apply> getApplyById(Integer current, Integer size, Integer userId) {
        Sort sort = Sort.by(Sort.Direction.DESC, "applyTime");
        Pageable pageable = PageRequest.of(current - 1, size, sort);
        return applyRepository.findAll(new Specification<Apply>() {
            @Override
            public Predicate toPredicate(Root<Apply> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Predicate temp = criteriaBuilder.equal(root.get("applyFromId"), userId);
                return temp;
            }
        }, pageable);
    }

    /**
     * 通过申请
     *
     * @param apply
     * @return
     */
    public Apply passApply(Apply apply) {
        CourseMember courseMember = new CourseMember();
        courseMember.setUserId(apply.getApplyFromId());
        courseMember.setIsManager(0);
        courseMember.setUserName(apply.getApplyUserName());
        courseMember.setCourseId(apply.getApplyToId());
        courseMember.setCreateTime(DateUtils.getNowTime());
        courseMember.setCourseName(courseService.getCoursesById(apply.getApplyToId()).getCourseName());

        courseMemberService.addCourseMember(courseMember);
        courseService.addStudentCount(apply.getApplyToId());

        apply.setIsPass(2);//通过申请
        return applyRepository.saveAndFlush(apply);
    }

    /**
     * 拒绝申请
     *
     * @param apply
     * @return
     */
    public Apply refuseApply(Apply apply) {
        apply.setIsPass(1);//拒绝申请
        return applyRepository.saveAndFlush(apply);
    }

    /**
     * 删除申请
     *
     * @param apply
     * @return
     */
    public boolean deleteApply(Apply apply) {
        try {
            applyRepository.deleteById(apply.getId());
            return true;
        } catch (RuntimeException e) {
            return false;
        }
    }

    /**
     * 根据apply_from_id删除申请
     *
     * @param applyFromId 用户id
     * @return
     */
    public boolean deleteApplyByApplyFromId(Integer applyFromId) {
        try {
            applyRepository.deleteByApplyFromId(applyFromId);
            return true;
        } catch (RuntimeException e) {
            return false;
        }
    }
}
