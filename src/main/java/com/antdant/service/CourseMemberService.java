package com.antdant.service;

import com.antdant.entity.CourseMember;
import com.antdant.repository.CourseMemberRepository;
import com.antdant.repository.CourseRepository;
import com.antdant.utils.DataUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author Lee Yunc
 * @Date 2021/3/2 10:01
 * @Description:
 * @Version 1.0
 */
@Slf4j
@Service
public class CourseMemberService {

    @Resource
    private CourseMemberRepository courseMemberRepository;

    @Resource
    private CourseService courseService;

    /**
     * 判断某名用户是否已经存在于某门课程中
     *
     * @param userId
     * @param courseId
     * @return
     */
    public boolean isInThisCourse(Integer userId, Integer courseId) {
        if (courseMemberRepository.findByUserIdAndCourseId(userId, courseId) != null) {
            return true;
        }
        return false;
    }

    /**
     * 管理页获取关联表数据
     *
     * @param current
     * @param size
     * @param courseMember
     * @return
     */
    public Page<CourseMember> getManageCourseMember(Integer current, Integer size, CourseMember courseMember) {
        Pageable pageable = PageRequest.of(current - 1, size);
        return courseMemberRepository.findAll(new Specification<CourseMember>() {
            @Override
            public Predicate toPredicate(Root<CourseMember> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> list = new ArrayList<>();
                if (DataUtils.isNotEmptyStr(courseMember.getCourseName())) {
                    list.add(criteriaBuilder.equal(root.get("courseName"), courseMember.getCourseName()));
                }
                if (DataUtils.isNotEmptyStr(courseMember.getUserName())) {
                    list.add(criteriaBuilder.equal(root.get("userName"), courseMember.getUserName()));
                }
                return criteriaBuilder.and(list.toArray(new Predicate[list.size()]));
            }
        }, pageable);
    }

    /**
     * 删除关联
     *
     * @param userId
     * @param courseId
     * @param isManager
     * @return
     */
    public boolean deleteRelate(Integer userId, Integer courseId, Integer isManager) {
        if (isManager == 0) {
            try {
                courseMemberRepository.deleteByUserIdAndCourseId(userId, courseId);
                courseService.subtractStudentCount(courseId);//课程人数减一
                return true;
            } catch (RuntimeException e) {
                log.error("删除关系失败");
                return false;
            }

        } else {
            boolean flag = false;
            //TODO 删除外键关系
            return flag;
        }
    }

    /**
     * 添加课程成员
     *
     * @param courseMember
     * @return
     */
    public CourseMember addCourseMember(CourseMember courseMember) {
        return courseMemberRepository.saveAndFlush(courseMember);
    }

    /**
     * 根据用户id 获取到该用户管理的课程id
     *
     * @param userId
     * @return
     */
    public Integer getCourseIdByUserId(Integer userId) {
        CourseMember courseMember = courseMemberRepository.findByUserIdAndIsManager(userId, 1);
        if (DataUtils.isNotNull(courseMember)) {
            return courseMember.getCourseId();
        }
        return 0;
    }
}
