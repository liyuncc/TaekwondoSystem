package com.antdant.service;

import com.antdant.entity.Account;
import com.antdant.entity.Member;
import com.antdant.repository.AccountRepository;
import com.antdant.repository.MemberRepository;
import com.antdant.utils.DataUtils;
import com.antdant.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author liyunc
 * @Date 2021/6/4 15:56
 * @Description:
 * @Version 1.0
 */
@Slf4j
@Service
public class MemberService {

    @Resource
    private UserService userService;

    @Resource
    private MemberRepository memberRepository;

    @Resource
    private AccountRepository accountRepository;

    /**
     * 添加新会员
     *
     * @param member
     * @return
     */
    public Member addNewMember(Member member) {
        Member newMember = createNewMember();
        newMember.setEndTime(member.getEndTime());
        newMember.setLoginId(member.getLoginId());
        newMember.setUserName(member.getUserName());
        newMember.setPhone(userService.getUserByLoginId(member.getLoginId()).getPhone());

        //TODO 将缴费记录写入财务表
//        Account account = new Account();
//        account.setLoginId(member.getLoginId());
//        account.setPhone(userService.getUserByLoginId(member.getLoginId()).getPhone());
//        account.setType(1);//1 会员续费
//        account.setUserName(member.getUserName());
//        account.setCreateTime(DateUtils.getNowTime());
//        account.setMoney(500);//会员续费，每次续费费用一定，为500元
//        accountRepository.saveAndFlush(account);
        addToAccount(member);

        return memberRepository.saveAndFlush(newMember);
    }

    /**
     * 初始化新会员
     *
     * @return
     */
    private Member createNewMember() {
        Member member = new Member();
        member.setLastTime(DateUtils.getNowTime());
        return member;
    }

    /**
     * 获取所有会员
     *
     * @param current
     * @param size
     * @param member
     * @return
     */
    public Page<Member> getAllMember(Integer current, Integer size, Member member) {
        Pageable pageable = PageRequest.of(current - 1, size);
        return memberRepository.findAll(new Specification<Member>() {
            @Override
            public Predicate toPredicate(Root<Member> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> list = new ArrayList<>();
                if (DataUtils.isNotEmptyStr(member.getLoginId())) {
                    list.add(criteriaBuilder.like(root.get("loginId"), member.getLoginId()));
                }
                if (DataUtils.isNotEmptyStr(member.getUserName())) {
                    list.add(criteriaBuilder.like(root.get("userName"), member.getUserName()));
                }
                return criteriaBuilder.and(list.toArray(new Predicate[list.size()]));
            }
        }, pageable);
    }

    /**
     * 会员续费
     *
     * @param memberId
     * @return
     */
    public Member renewMember(Integer memberId) {
        Member member = memberRepository.findById(memberId).orElse(null);
        member.setLastTime(DateUtils.getNowTime());
        member.setEndTime(DateUtils.getNowTime());

        //TODO 将续费记录写入财务表
//        Account account = new Account();
//        account.setLoginId(member.getLoginId());
//        account.setPhone(userService.getUserByLoginId(member.getLoginId()).getPhone());
//        account.setType(1);//1 会员续费
//        account.setUserName(member.getUserName());
//        account.setCreateTime(DateUtils.getNowTime());
//        account.setMoney(500);//会员续费，每次续费费用一定，为500元
//        accountRepository.saveAndFlush(account);
        addToAccount(member);

        return memberRepository.saveAndFlush(member);
    }

    /**
     * 将缴费、续费记录加入到财务账单
     *
     * @param member
     */
    private void addToAccount(Member member) {
        Account account = new Account();
        account.setLoginId(member.getLoginId());
        account.setPhone(userService.getUserByLoginId(member.getLoginId()).getPhone());
        account.setType(1);//1 会员续费
        account.setUserName(member.getUserName());
        account.setCreateTime(DateUtils.getNowTime());
        account.setMoney(500);//会员续费，每次续费费用一定，为500元
        accountRepository.saveAndFlush(account);
    }
}
