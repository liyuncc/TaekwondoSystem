package com.antdant.service;

import com.antdant.entity.Activity;
import com.antdant.repository.ActivityRepository;
import com.antdant.utils.DataUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author Lee Yunc
 * @Date 2021/3/8 10:22
 * @Description:
 * @Version 1.0
 */
@Service
public class ActivityService {

    @Resource
    private ActivityRepository activityRepository;

    /**
     * 获取活动列表
     *
     * @param current
     * @param size
     * @param courseId
     * @return
     */
    public Page<Activity> getAllActivity(Integer current, Integer size, Integer courseId) {
        Sort sort = Sort.by(Sort.Direction.DESC, "createTime");
        Pageable pageable = PageRequest.of(current - 1, size, sort);
        return activityRepository.findAll(new Specification<Activity>() {
            @Override
            public Predicate toPredicate(Root<Activity> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Predicate predicate = null;
                if (courseId > 0) {
                    predicate = criteriaBuilder.equal(root.get("courseId"), courseId);
                }
                return predicate;
            }
        }, pageable);
    }

    /**
     * 根据活动id  获取活动详情
     *
     * @param activityId
     * @return
     */
    public Activity getActivityById(Integer activityId) {
        return activityRepository.findById(activityId).orElse(null);
    }

    public Page<Activity> getManageActivity(Integer current, Integer size, Activity activity) {
        Sort sort = Sort.by(Sort.Direction.DESC, "createTime");
        Pageable pageable = PageRequest.of(current - 1, size, sort);
        return activityRepository.findAll(new Specification<Activity>() {
            @Override
            public Predicate toPredicate(Root<Activity> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> list = new ArrayList<>();
                if (DataUtils.isNotEmptyStr(activity.getActivityName())) {
                    list.add(criteriaBuilder.like(root.get("activityName"), activity.getActivityName()));
                }
                if (DataUtils.isNotEmptyStr(activity.getCourseName())) {
                    list.add(criteriaBuilder.like(root.get("courseName"), activity.getCourseName()));
                }
                return null;
            }
        }, pageable);
    }

    /**
     * 根据id删除活动
     *
     * @param activityId
     * @return
     */
    public boolean deleteActivity(Integer activityId) {
        try {
            activityRepository.deleteById(activityId);
            return true;
        } catch (RuntimeException e) {
            return false;
        }
    }

    /**
     * 新增活动
     *
     * @param activity
     * @return
     */
    public Activity addNewActivity(Activity activity) {
        return activityRepository.saveAndFlush(activity);
    }
}
