package com.antdant.utils;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author Lee Yunc
 * @Date 2021/2/23 21:02
 * @Description: ip工具类
 * @Version 1.0
 */
public class IpUtils {

    public static final String UN_KNOWN = "unknown";

    public static final String LOCAL_IP = "0:0:0:0:0:0:0:1";

    public static final String LOCAL_IP_STR = "127.0.0.1";

    public static final String[] ADDR_HEADER = {"X-Forwarded-For", "Proxy-Client-IP", "WL-Proxy-Client-IP", "X-Real-IP"};

    public static final String USER_AGENT = "user-agent";

    public static String getUserAgent(HttpServletRequest request) {
        return request.getHeader(USER_AGENT);
    }

    public static String getIP(HttpServletRequest request) {
        String ip = null;
        if (request instanceof HttpServletRequest) {
            HttpServletRequest hsr = request;
            for (String header : ADDR_HEADER) {
                if (DataUtils.isEmptyStr(ip) || UN_KNOWN.equalsIgnoreCase(ip)) {
                    ip = hsr.getHeader(header);
                } else {
                    break;
                }
            }
        }
        if (DataUtils.isEmptyStr(ip) || UN_KNOWN.equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        } else {
            // 对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按','分割
            int i = ip.indexOf(",");
            if (i > 0) {
                ip = ip.substring(0, i);
            }
        }
        if (!checkIP(ip)) {
            ip = LOCAL_IP_STR;
        }
        if (LOCAL_IP.equals(ip)) {
            ip = LOCAL_IP_STR;
        }
        return ip;
    }

    /**
     * 检查一个IP的方法
     *
     * @param ip
     * @return
     */
    private static boolean checkIP(String ip) {
        if (ip == null || ip.length() == 0 || UN_KNOWN.equalsIgnoreCase(ip)) {
            return false;
        }
        return true;
    }
}
