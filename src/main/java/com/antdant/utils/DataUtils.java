package com.antdant.utils;

import org.springframework.util.StringUtils;

/**
 * @Author Lee Yunc
 * @Date 2021/2/23 14:49
 * @Description: 数据判断工具类
 * @Version 1.0
 */
public class DataUtils {
    public static boolean isNotEmptyStr(String originalStr) {
        return !isEmptyStr(originalStr);
    }

    public static boolean isEmptyStr(String originalStr) {
        return StringUtils.isEmpty(originalStr);
    }

    public static boolean isNull(Object originalData) {
        return originalData == null;
    }

    public static boolean isNotNull(Object originalData) {
        return !isNull(originalData);
    }

    public static boolean isEqual(Object sourceData, Object targetData) {
        return sourceData.equals(targetData);
    }

    public static boolean isNotEqual(Object sourceData, Object targetData) {
        return !isEqual(sourceData, targetData);
    }

    public static String limitLength(String str, int length) {
        if (isEmptyStr(str)) {
            return str;
        }
        return str.length() > length ? str.substring(0, length) : str;
    }
}
