package com.antdant.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Author Lee Yunc
 * @Date 2021/3/6 11:52
 * @Description: 日期格式化工具
 * @Version 1.0
 */
public class DateUtils {

    /**
     * 英文全称（默认） 如：2021-3-6 12:00:31
     */
    public static String FORMAT_DATE_LONG = "yyyy-MM-dd HH:mm:ss";

    /**
     * 获得默认的 date pattern
     */
    public static String getDatePattern() {
        return FORMAT_DATE_LONG;
    }

    /**
     * 根据预设格式返回当前日期
     *
     * @return
     */
    public static String getNowTime() {
        return format(new Date());
    }

    /**
     * 根据用户格式返回当前日期
     *
     * @param format
     * @return
     */
    public static String getNowTime(String format) {
        return format(new Date(), format);
    }

    /**
     * 使用预设格式格式化日期
     *
     * @param date
     * @return
     */
    public static String format(Date date) {
        return format(date, getDatePattern());
    }

    /**
     * 使用用户格式格式化日期
     *
     * @param date    日期
     * @param pattern 日期格式
     * @return
     */
    public static String format(Date date, String pattern) {
        String returnValue = "";
        if (date != null) {
            SimpleDateFormat df = new SimpleDateFormat(pattern);
            returnValue = df.format(date);
        }
        return returnValue;
    }
}
