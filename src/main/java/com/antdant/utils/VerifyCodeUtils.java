package com.antdant.utils;

import java.util.Random;

/**
 * @Author Lee Yunc
 * @Date 2021/2/24 15:38
 * @Description: 字符验证码生成工具
 * @Version 1.0
 */
public class VerifyCodeUtils {
    //随机产生只有数字的字符串
    private static char[] randString = {
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
            'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

    public static String generateVerifyCode() {
        String verifyCode = "";
        Random random = new Random();
        for (int i = 0; i < 6; i++) {
            verifyCode += randString[random.nextInt(36)];
        }
        return verifyCode;
    }
}
