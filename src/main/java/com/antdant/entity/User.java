package com.antdant.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * @Author Lee Yunc
 * @Date 2021/2/7 14:12
 * @Description: 学员信息
 * @Version 1.0
 */
// id 学员id
// loginId 登录id（学号）
// username 用户名
// password 密码
// avatar 头像
// introduce 个人简介
// phone 电话
// email 邮箱
// sex 性别
// homeAddress 家庭住址
// createTime 注册时间
// rankCode 跆拳道段位
// point 积分
// isManager 是否为管理员（0：不是（学生） 1：是（教练） 2：系统管理员）

@Entity
@Data
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String loginId;

    private String username;

    private String password;

    private String avatar;

    private String introduce;

    private String phone;

    private String email;

    private Integer sex;

    private String homeAddress;

    private String createTime;

    private Integer rankCode;

    private Integer point;

    private Integer isManager;
}
