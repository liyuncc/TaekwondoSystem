package com.antdant.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * @Author Lee Yunc
 * @Date 2021/2/7 14:14
 * @Description: 报名申请
 * @Version 1.0
 */
// id  申请id
// applyFromId  申请人id
// applyUsername  申请人姓名
// applyToId  申请的id
// reason  申请理由
// applyTime  申请时间
// contactWay  联系方式
// isPass  是否通过申请(0：等待处理 1：拒绝 2：通过)

@Entity
@Data
public class Apply implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer applyFromId;

    private String applyUserName;

    private Integer applyToId;

    private String reason;

    private String applyTime;

    private String contactWay;

    private Integer isPass = 0;
}
