package com.antdant.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * @Author Lee Yunc
 * @Date 2021/3/17 16:42
 * @Description:
 * @Version 1.0
 */
//id 主键
//name 姓名
//number 证书编号
//birthday 出生日期
//grade 级位
//examiner 考试官
//date 发证日期
//image 证书图片

@Entity
@Data
public class Certificate implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private String number;

    private String birthday;

    private String grade;

    private Integer rankCode;

    private String examiner;

    private String date;

    private String image;

}
