package com.antdant.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * @Author liyunc
 * @Date 2021/6/5 13:04
 * @Description: 公告信息
 * @Version 1.0
 */

// id 公告id
// content 公告内容
// examTime 考级日期
// notice 考级要求
// pLace 考级地点
// documents 准备资料
// 创建时间
@Entity
@Data
public class Inform implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String content;

    private String examTime;

    private String notice;

    private String place;

    private String documents;

    private String createTime;
}
