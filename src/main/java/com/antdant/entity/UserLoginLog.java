package com.antdant.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.criteria.CriteriaBuilder;
import java.io.Serializable;

/**
 * @Author Lee Yunc
 * @Date 2021/2/23 17:34
 * @Description: 用户登录日志
 * @Version 1.0
 */
// id 记录id
// userLoginId 用户登录id
// userToken 用户token
// createTime 创建时间
// operationType 操作类型（1：登录 2：登出）
// ip ip地址
// userAgent 用户代理
// operatingSystemVersion 操作系统信息
// browserVersion 浏览器信息

@Entity
@Data
public class UserLoginLog implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String userLoginId;

    private String userToken;

    private String createTime;

    private Integer operationType;

    private String ip;

    private String userAgent;

    private String operatingSystemVersion;

    private String browserVersion;


}
