package com.antdant.entity;


import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * @Author Lee Yunc
 * @Date 2021/2/7 14:13
 * @Description: 评论
 * @Version 1.0
 */
// id  留言id
// courseId  被留言课程id
// userId  留言用户id
// userName  留言用户姓名
// avatar  留言用户头像
// isManager  是否是管理员
// content  留言内容
// supportCount  获赞数量
// replyCount  回复数量
// createTime  创建时间

@Entity
@Data
public class Comment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer courseId;

    private Integer userId;

    private String userName;

    private String avatar;

    private Integer isManager;

    private String content;

    private Integer supportCount;

    private Integer replyCount;

    private String createTime;


}
