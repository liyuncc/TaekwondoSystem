package com.antdant.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * @Author Lee Yunc
 * @Date 2021/2/7 14:13
 * @Description: 回复
 * @Version 1.0
 */
// id 回复id
// userId 用户id
// username 用户名
// avatar 用户头像
// isManager 是不是管理员
// content 评论内容
// replyToId 针对评论的id
// supportCount 点赞数量
// createTime 评论时间

@Entity
@Data
public class Reply implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer userId;

    private String userName;

    private String avatar;

    private Integer isManager;

    private String content;

    private Integer replyToId;

    private Integer supportCount;

    private String createTime;
}
