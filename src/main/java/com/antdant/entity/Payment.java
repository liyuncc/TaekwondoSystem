package com.antdant.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * @Author liyunc
 * @Date 2021/6/4 22:50
 * @Description:
 * @Version 1.0
 */

// id 续费id
// loginId 用户学号
// userName 用户姓名
// phone 电话号码
// lastTime 上次缴费日期
// lastType 上次缴费类型
// endTime 费用截止日期

@Entity
@Data
public class Payment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String loginId;

    private String userName;

    private String phone;

    private String lastTime;

    private Integer lastType;

    private String endTime;
}
