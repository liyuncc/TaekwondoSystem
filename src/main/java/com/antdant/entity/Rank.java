package com.antdant.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * @Author Lee Yunc
 * @Date 2021/3/8 16:16
 * @Description: 跆拳道等级
 * @Version 1.0
 */

// id  级别主键
// rankCode  级别段位码
// grade 段位
// beltName  色带名称
// beltColor  腰带颜色
// requiredGoodPotential  晋段必修品势
// ageLimit  年龄限制
// note  备注
// moral  寓意
@Entity
@Data
public class Rank implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer rankCode;

    private String grade;

    private String beltName;

    private String beltColor;

    private String requiredGoodPotential;

    private String ageLimit;

    private String note;

    private String moral;
}
