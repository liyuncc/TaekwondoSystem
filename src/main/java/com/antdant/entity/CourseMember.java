package com.antdant.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * @Author Lee Yunc
 * @Date 2021/3/1 11:14
 * @Description:
 * @Version 1.0
 */

//id
//userLoginId 用户登录id（学号）
//isManager 是否是管理员（0：是 1：否）
//userName 用户姓名
//courseId 课程id
//courseName 课程名称
//createTime 创建时间

@Entity
@Data
public class CourseMember implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer userId;

    private Integer isManager;

    private String userName;

    private Integer courseId;

    private String courseName;

    private String createTime;
}
