package com.antdant.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @Author Lee Yunc
 * @Date 2021/2/7 14:14
 * @Description: 活动
 * @Version 1.0
 */
// id  活动id
// courseId //举办活动的课程id（选填）
// courseName //课程名称（选填）
// activityName  活动名称
// createTime  创建时间
// address  地址
// money  活动经费
// description  活动介绍
// summary  活动简介
// photos  活动图片
// point  活动积分

@Entity
@Data
public class Activity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer courseId;//举办活动的课程id

    private String courseName;//课程名称

    private String activityName;

    private String createTime;

    private String address;

    private String money;

    private String description;

    private String summary;

    private String photos;

    private Integer point;

}
