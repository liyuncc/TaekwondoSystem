package com.antdant.entity;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * @Author Lee Yunc
 * @Date 2021/2/7 14:15
 * @Description: 课程
 * @Version 1.0
 */
// id 课程id
// courseName 课程名称
// theme 课程主题(课程宗旨)
// descriptions 课程描述
// studentCount 学生数量
// courseManagerId 课程管理员id
// createTime 创建时间
// type 课程类型（1：腿法 2：拳法 3：步法 4：品势）
// level 课程等级
// themeImg 主题照片
// contact 课程群聊联系方式
// address 上课地址（教室位置）
// classTime 上课时间

@Entity
@Data
public class Course implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String courseName;

    private String theme;

    private String descriptions;

    private Integer studentCount;

    private Integer courseManagerId;

    private String createTime;

    private Integer type;

    private Integer level;

    private String themeImg;

    private String contact;

    private String address;

    private String classTime;

}
