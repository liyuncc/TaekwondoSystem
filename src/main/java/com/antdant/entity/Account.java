package com.antdant.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * @Author Lee Yunc
 * @Date 2021/2/7 14:15
 * @Description: 账单详情
 * @Version 1.0
 */
// id 账单id
// loginId 用户学号
// userName 用户姓名
// phone 电话号码
// type 账单类型（0：会员续费 1：学费缴纳 2：教具购买）
// money 费用
// createTime 创建时间

@Entity
@Data
public class Account implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String loginId;

    private String userName;

    private String phone;

    private Integer type;

    private Integer money;

    private String createTime;

}
