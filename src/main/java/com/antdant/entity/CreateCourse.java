package com.antdant.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * @Author Lee Yunc
 * @Date 2021/3/4 10:35
 * @Description: 创建课程申请表
 * @Version 1.0
 */
//id 申请表id
//applyFromId 申请人id
//applyToName 申请创建课程名称
//reason 申请原因
// courseType 课程类别
// contact 联系方式
//createTime 创建时间
//isPass 是否通过该请求（0：未处理 1：拒绝 2：通过）

@Entity
@Data
public class CreateCourse implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer applyFromId;

    private String applyToName;

    private String reason;

    private Integer courseType;

    private String contact;

    private String createTime;

    private Integer isPass;
}
