package com.antdant.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * @Author Lee Yunc
 * @Date 2021/2/7 14:20
 * @Description: 奖品
 * @Version 1.0
 */
// id 奖品id
// prizeName 奖品名称
// point 奖品兑换所需积分
// description 奖品描述
// image 图片
// createTime 创建时间

@Entity
@Data
public class Prize implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String prizeName;

    private String description;

    private String image;

    private String createTime;
}
