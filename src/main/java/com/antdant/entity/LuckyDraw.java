package com.antdant.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * @Author Lee Yunc
 * @Date 2021/2/7 15:16
 * @Description: 积分抽奖兑换
 * @Version 1.0
 */
// id 兑换id
// userId 用户id
// userName 用户姓名
// prizeId 奖品id
// prizeName 奖品名称
// createTime 创建时间
// redeemTime 兑换时间
// status 兑换状态（0：未领取 1：已领取）

@Entity
@Data
public class LuckyDraw implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer userId;

    private String userName;

    private Integer prizeId;

    private String prizeName;

    private String createTime;

    private Integer status;

    private String redeemTime;

}
