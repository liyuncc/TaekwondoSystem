package com.antdant.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * @Author liyunc
 * @Date 2021/5/24 18:41
 * @Description: 教具采购
 * @Version 1.0
 */
// id 缴费id
// loginId 用户学号
// userName 用户姓名
// phone 电话号码
// money 费用
// endTime 费用截止日期

@Entity
@Data
public class Purchase implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String loginId;

    private String userName;

    private String phone;

    private String goodsName;

    private Integer money;

    private String createTime;
}
