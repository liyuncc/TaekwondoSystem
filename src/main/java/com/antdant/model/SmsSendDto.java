package com.antdant.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author Lee Yunc
 * @Date 2021/2/24 15:54
 * @Description: 验证信息
 * @Version 1.0
 */
@Data
public class SmsSendDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String fromUser;

    private String toUser;

    private String subject;

    private String textContent;

}
