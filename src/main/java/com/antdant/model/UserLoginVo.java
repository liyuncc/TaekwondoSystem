package com.antdant.model;

import com.antdant.entity.User;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author Lee Yunc
 * @Date 2021/2/23 15:18
 * @Description: 用户登录信息
 * @Version 1.0
 */
@Data
public class UserLoginVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private User user;

    private String userToken;

    public static UserLoginVo factory(User user, String userToken) {
        UserLoginVo studentLoginVo = new UserLoginVo();
        studentLoginVo.setUser(user);
        studentLoginVo.setUserToken(userToken);
        return studentLoginVo;
    }
}
