package com.antdant.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Author Lee Yunc
 * @Date 2021/2/24 20:38
 * @Description: 注入当前登录用户注解
 * @Version 1.0
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface RequireLogin {
    /**
     * 必须登录，默认 true
     *
     * @return
     */
    boolean required() default true;
}
