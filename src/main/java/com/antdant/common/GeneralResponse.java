package com.antdant.common;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author Lee Yunc
 * @Date 2021/2/22 11:03
 * @Description:
 * @Version 1.0
 */
@Data
public class GeneralResponse implements Serializable {
    /**
     * 状态码
     */
    private int code;

    /**
     * 响应消息
     */
    private String message;

    /**
     * 响应对象
     */
    private Object data;

    /**
     * 时间戳
     */
    private long time;

    public GeneralResponse() {
        super();
    }

    public GeneralResponse(GeneralCode code, String message, Object data) {
        this.code = code.getCode();
        this.message = message;
        this.data = data;
        this.time = System.currentTimeMillis();
    }

    public GeneralResponse(GeneralCode code, Object data) {
        this.code = code.getCode();
        this.message = code.getMessage();
        this.data = data;
        this.time = System.currentTimeMillis();
    }

    public GeneralResponse(int code, String message) {
        this.code = code;
        this.message = message;
        this.time = System.currentTimeMillis();
    }

    //成功返回消息和数据
    public static GeneralResponse success(String message, Object data) {
        return new GeneralResponse(GeneralCode.SUCCESS, message, data);
    }

    //成功返回数据
    public static GeneralResponse success(Object data) {
        return new GeneralResponse(GeneralCode.SUCCESS, data);
    }

    //成功返回消息
    public static GeneralResponse success(String message) {
        return new GeneralResponse(GeneralCode.SUCCESS, message, "");
    }

    //成功返回
    public static GeneralResponse success() {
        return new GeneralResponse(GeneralCode.SUCCESS, "");
    }

    //失败返回数据和消息
    public static GeneralResponse failed(String message, Object data) {
        return new GeneralResponse(GeneralCode.FAILED, message, data);
    }

    //失败返回信息和状态码
    public static GeneralResponse failed(int code, String message) {
        return new GeneralResponse(code, message);
    }

    public static GeneralResponse failed(GeneralCode code, String message) {
        return new GeneralResponse(code, message, "");
    }

    //失败返回消息
    public static GeneralResponse failed(String message) {
        return new GeneralResponse(GeneralCode.FAILED, message, "");
    }

    //失败返回数据
    public static GeneralResponse failed(Object data) {
        return new GeneralResponse(GeneralCode.FAILED, data);
    }

    //失败返回
    public static GeneralResponse failed() {
        return new GeneralResponse(GeneralCode.FAILED, "");
    }

}
