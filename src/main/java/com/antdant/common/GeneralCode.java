package com.antdant.common;

/**
 * @Author Lee Yunc
 * @Date 2021/2/22 11:24
 * @Description:
 * @Version 1.0
 */
public enum GeneralCode {

    SUCCESS(0, "操作成功"),
    FAILED(-1, "操作失败"),
    SYSTEM_EXCEPTION(500, "System Error"),
    VALID_ERROR(-1001, "验证失败"),
    OBJECT_NOT_EXISTS(-1002, "对象不存在"),
    OBJECT_HAS_EXISTS(-1003, "对象已存在"),
    NEED_LOGIN(401, "需要登录"),
    RESOURCE_NOT_FOUND(404, "Not Found");

    private int code;
    private String message;

    GeneralCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
