package com.antdant.constant;

/**
 * @Author Lee Yunc
 * @Date 2021/2/24 10:54
 * @Description:
 * @Version 1.0
 */
public enum LoginOperationTypeEnum {
    LOGIN(1, "login"),
    LOGOUT(-1, "logout");

    private int operationType;
    private String operationTypeStr;

    LoginOperationTypeEnum(int operationType, String operationTypeStr) {
        this.operationType = operationType;
        this.operationTypeStr = operationTypeStr;
    }

    public int getOperationType() {
        return operationType;
    }

    public String getOperationTypeStr() {
        return operationTypeStr;
    }
}
