package com.antdant.constant;

/**
 * @Author Lee Yunc
 * @Date 2021/2/22 18:02
 * @Description: 跆拳道段位（十级九段）
 * @Version 1.0
 */
public enum RankEnum {

    //WHITE_BELT("级别","色带名称","腰带颜色","晋段必修品势","年龄限制","备注","寓意");
    WHITE_BELT(10, "白带", "None/White", "", "15岁或以下", "",
            "表示空白，尚未具有跆拳道知识，意味着入门阶段。有纯洁之意，穿戴者对跆拳道的技术和知识一窍不通，尚待磨练。"),
    WHITE_AND_YELLOW_BELT(9, "白黄带", "White/White with stripe", "", "15岁或以下", "",
            "白带与黄带之交界，一半白色一半黄色，这就代表着练习者已经开始接触跆拳道并学习了一些初级内容，开始向黄带过渡。"),
    YELLOW_BELT(8, "", "", "", "", "", ""),
    YELLOW_AND_GREEN_BELT(7, "", "", "", "", "", ""),
    GREEN_BELT(6, "", "", "", "", "", ""),
    GREEN_AND_BLUE_BELT(5, "", "", "", "", "", ""),
    BLUE_BELT(4, "", "", "", "", "", ""),
    BLUE_AND_RED_BELT(3, "", "", "", "", "", ""),
    RED_BELT(2, "", "", "", "", "", ""),
    RED_AND_BLACK_BELT(1, "", "", "", "", "", ""),

    FIRST_GOOD_POTENTIAL(101, "", "", "", "", "", ""),
    SECOND_GOOD_POTENTIAL(102, "", "", "", "", "", ""),
    THIRD_GOOD_POTENTIAL(103, "", "", "", "", "", ""),

    BLACK_BELT_ONE_SEGMENT(1001, "", "", "", "", "", ""),
    BLACK_BELT_TWO_SEGMENT(1002, "", "", "", "", "", ""),
    BLACK_BELT_THREE_SEGMENT(1003, "", "", "", "", "", ""),
    BLACK_BELT_FOUR_SEGMENT(1004, "", "", "", "", "", ""),
    BLACK_BELT_FIVE_SEGMENT(1005, "", "", "", "", "", ""),
    BLACK_BELT_SIX_SEGMENT(1006, "", "", "", "", "", ""),
    BLACK_BELT_SEVEN_SEGMENT(1007, "", "", "", "", "", ""),
    BLACK_BELT_EIGHT_SEGMENT(1008, "", "", "", "", "", ""),
    BLACK_BELT_NINE_SEGMENT(1009, "", "", "", "", "", "");

    private int rank;
    private String beltName;
    private String beltColor;
    private String requiredGoodPotential;
    private String ageLimit;
    private String note;
    private String moral;

    RankEnum(int rank, String beltName, String beltColor, String requiredGoodPotential, String ageLimit, String note, String moral) {
        this.rank = rank;
        this.beltName = beltName;
        this.beltColor = beltColor;
        this.requiredGoodPotential = requiredGoodPotential;
        this.ageLimit = ageLimit;
        this.note = note;
        this.moral = moral;
    }

    @Override
    public String toString() {
        return "TaeKWonDoLevelEnum{" +
                "rank=" + rank +
                ", beltName='" + beltName + '\'' +
                ", beltColor='" + beltColor + '\'' +
                ", requiredGoodPotential='" + requiredGoodPotential + '\'' +
                ", ageLimit='" + ageLimit + '\'' +
                ", note='" + note + '\'' +
                ", moral='" + moral + '\'' +
                '}';
    }



    public int getRank() {
        return rank;
    }

    public String getBeltName() {
        return beltName;
    }

    public String getBeltColor() {
        return beltColor;
    }

    public String getRequiredGoodPotential() {
        return requiredGoodPotential;
    }

    public String getAgeLimit() {
        return ageLimit;
    }

    public String getNote() {
        return note;
    }

    public String getMoral() {
        return moral;
    }
}
