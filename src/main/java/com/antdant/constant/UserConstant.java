package com.antdant.constant;

/**
 * @Author Lee Yunc
 * @Date 2021/2/22 13:12
 * @Description:
 * @Version 1.0
 */
public class UserConstant {

    //登录缓存前缀
    public static final String USER_TOKEN_CACHE_KEY_PREFIX = "login_user_token:";

    //登录token超时时间 1小时
    public static final long USER_TOKEN_EXPIRE = 1 * 1 * 60 * 60;

    //请求头token存放KEY
    public static final String USER_TOKEN_HEADER_KEY = "Authorization";

}
