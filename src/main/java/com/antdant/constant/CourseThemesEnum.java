package com.antdant.constant;

/**
 * @Author Lee Yunc
 * @Date 2021/2/7 17:56
 * @Description: 课程主题常量
 * @Version 1.0
 */
public enum CourseThemesEnum {

    LEVEL_FIRST_COURSE_THEMES(1001, "a"),
    LEVEL_SECOND_COURSE_THEMES(1002, "a"),
    LEVEL_THIRD_COURSE_THEMES(1003, "a"),
    LEVEL_FOURTH_COURSE_THEMES(1004, "a"),
    LEVEL_FIFTH_COURSE_THEMES(1005, "a");

    private int level;
    private String theme;

    CourseThemesEnum(int level, String theme) {
        this.level = level;
        this.theme = theme;
    }

    public int getLevel() {
        return level;
    }

    public String getTheme() {
        return theme;
    }
}
