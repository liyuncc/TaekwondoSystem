package com.antdant;

import com.antdant.constant.RankEnum;
import com.antdant.utils.DateUtils;

import java.util.Date;

/**
 * @Author Lee Yunc
 * @Date 2021/2/22 13:03
 * @Description:
 * @Version 1.0
 */
public class Test {
    private static int a;


    public static void main(String[] args) {
        methodName(a);

        System.out.println(RankEnum.WHITE_BELT);

        System.out.println(new Date());
        System.out.println(DateUtils.getNowTime());
        System.out.println(a);
    }

    private static void methodName(int b) {
        a++;
    }
}
